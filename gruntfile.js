module.exports = function (grunt){
	grunt.initConfig({
		imagemin: {
			dynamic: {
				files: [{
					expand: true,
					cwd: 'components/images/',
					src: '**/*.{png,jpg}',
					dest: 'builds/development/images'
				}]
			}
		},
		wiredep: {
			task: {
				src: 'builds/development/**/*.html'
			}
		},
		sass: {
			dist: {
				options: {
					style: 'expanded'
				},
				files: [{
					src: 'components/sass/style.scss',
					dest:'builds/development/css/chat-style.css'
				}]
			}
		},
		csscomb: {
			options: {
				config: '.csscomb.json'
			},
			style: {
				files :{
					'builds/development/css/chat-style.css' : ['builds/development/css/chat-style.css'] 
				}
			}
		},
		watch: {
			scripts: {
				files: ['builds/development/**/*.html',
				'builds/development/js/**/*.js',
				'components/sass/**/*.scss'
				],
				tasks: ['sass']
			},
			options: {
				spawn: false,
				livereload: true
			}
		},
		connect: {
			server : {
				options: {
					port: 3000,
					hostname: 'localhost',
					livereload: true,
					base: 'builds/development/'
				}
			}
		}
	});
	grunt.loadNpmTasks('grunt-contrib-imagemin');
	grunt.loadNpmTasks('grunt-wiredep');
	grunt.loadNpmTasks('grunt-sass');
	grunt.loadNpmTasks('grunt-csscomb');
	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-connect');

	grunt.registerTask('default',['sass','csscomb','wiredep','connect','watch']);
}